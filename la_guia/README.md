# Guia per l'apoderament tecnològic als municipis

## Intro
La Guia per l'apoderament tecnològic als municipis és un document que pretén avançar en la Sobirania Tecnològica a través de la implementació de polítiques municipals concretes. La voluntat d'aquest document és el d'impregnar de sobirania tecnologica els programes dels partits polítics que concorreran a les eleccions municipals del 2019, no només dedicant un apartat especific dels seus programes a les propostes que farem a continuació, si no aplicant al programa sencer una nova mirada.

Tot i la clara aposta d'aquesta guia per un canvi en les polítiques municipals, el procés per avançar cap a aquesta sobirania, no s'ha d'entendre com un procés exclusivament liderat per l'administració pública, sino com un fet compartit entre els diferents agents d'un territori. És per això que entenem aquesta guia com un full de ruta que ens ha de servir a les diferents entitats que treballem aquest tema, per poder fer un seguiment i revisió de la nostra pròpia feina.


## Argumentari

La tecnologia, o l'activitat tecnològica d'una regió, influeix en el progrés econòmic i social d'aquesta.  És per això que calen unes polítiques municipals que donin als municipis la capacitat de ser protagonistes de la seva activitat tecnològica i que no es limitin, com passa en l'actualitat, només a potenciar l'evolució tecnològica orientada per l'interès privat de les empreses líders del sector. Aquest protagonisme ha de servir per fer créixer una tecnologia oberta, de fàcil accés i sota control democràtic.

A més, la tecnologia ha inundat tots els aspectes de la vida diària de les persones, facilitant o limitant les llibertats personals. Per això. cal que l'administració no se'n desentengui i no només reguli adequadament la l'us de les tecnologies en aquests àmbits, sinó que també sigui exemplar vetllant per les llibertats dels ciutadans.

No s'ha de confondre el fet de ser sobirans tecnològicament, amb una administració pública que controli la tecnologia, sino tot el contrari. La sobirania tecnològica, s'ha de basar en tecnologies lliures: de lliure accés, ús, estudi i rèplica, que evitin, pricisament, el control tecnològic. A més a més, la naturalesa d'aquestes tecnologies, propicia que el seu disseny i desenvolupament es faci a través de xarxes d'agents on es poden combinar les capacitats tècniques amb el coneixement de les necessitats del territori, i on no recaigui únicament sobre l'administació pública la responsabilitat de fer evolucionar la tecnologia.


## XXX mesures per a l'apoderament tecnològic als municipis

### Infraestructura de telecomunicacions
La infraestructura de telecomunicacions és tot aquell cablejat i maquinari que fa possible que la informació viatgi d'un lloc a un altre. És el pilar d'Internet. Actualment tenim infraestructures públiques, privades i procomunes, segons quin el seu propietari i quin model de gestió té.
Les decisions que es prenen relatives a la regulació, evolució o ampliació d'aquesta infraestructura han sigut objecte de fortes tensions entre diferents interessos privats que busquen controlar la xarxa i la  informació que hi circula per poder extreure'n un benefici. També hem pogut veure com el control d'aquesta xarxa s'ha utilitzat dins de campanyes de censura.


#### Objectius generals
* Tendir cap a la universalització i sobirania de les xarxes de telecomunicacions, potenciant l'ús neutre de les xarxes de telecomunicacions.
* Impulsar que el desplegament del 5G es faci a través d'una xarxa compartida pels diferents operadors.
* Potenciar les operadores de proximitat. Evitant el monopoli de les grans operadores, incrementant-ne el seu control.
* Desplegament i consolidació una xarxa de dades d’Internet de les coses oberta, lliure i neutral. creada col·lectivament des de baix.


#### Proposta programàtica

##### Consolidació d'un ecosistema d'infrastructura col·laborativa i oberta de tecnologies lliures
1. Proporcionar accés real a la ciutadania i a la societat en general a una oferta assequible i variada de serveis de telecomunicacions de la màxima qualitat, capacitat i neutral.
2. Promoure ordenances municipals de desplegament de nova infraestructura com la proposada per Guifi.net, que promou amb la màxima agilitat i eficiència possible, estimula i maximitza l'eficiència de qualsevol tipus d'inversió i, alhora assegura la seva sostenibilitat en base a l'ús que se'n fa, minimitzant el cost per a l'administració pública i també per al ciutadà i la societat en general.
3. Afavorir les xarxes comunals i obertes, incidint en la necessitat d'evaluació del impacte social i territorial dels desplegaments, tenint en compte els possibles casos d'ús.

##### Desplegament de la xarxa mòbil de cinquena generació
4. Impulsar que el desplegament del 5G es faci a través d'una xarxa compartida pels diferents operadors.
5. Facilitar la implantació d'antenes en edificis públics.

##### Contractació de serveis de telecomunicacions
6. Contractar serveis a companyies de telecomunicacions que no discriminin el trànsit, el filtrin o l'interrompin, com en el cas del referendum d'autodeterminació de l'1 d'octubre de 2017.

##### Internet dels Objectes
7. Promoure les iniciatives de xarxes internet dels objectes oberta, lliure i neutral, [mitjançant ...]
8. Contribuïr a una arquitectura d'internet dels objectes per les ciutats que sigui oberta i interoperable.


### Programari Lliure
Quan un programa informàtic és pot utilitzar sense cap restricció, modificar segons les pròpies necessitats, compartir amb qui també el necessita, i estudiar per entendre com funciona, llavors l'anomenem lliure. EL programari lliure permet que, mitjançant la cooperació, la coproducció i la compartició de coneixements i recursos, es puguin resoldre necessitats comunes. També evita la dependència a l'empresa propietària del programa i possibilita verificar -en termes de seguretat- que el programa només fa el que ha de fer.

#### objectiu general de l'àmbit
* No hostilitat cap al programari lliure
  * Evitar cap dificultat artificial en l'ús de programari lliure per part de la ciutadania
  * No obligar mai la ciutadania a utilitzar tecnologies d'un fabricant concret, o serveis que espiïn, que obliguin a donar més dades de les necessàries, o que utilitzin l'activitat de les usuàries per fer negoci.
* Utilitzar el màxim de programari lliure per part de l'ajuntament
  * Aconseguir que tot el codi finançat per la gent estigui a disposició de la gent.
  * Que l'administració local tingui ple control i capacitat de decisió sobre quines tecnologies implanta, com gestiona les dades que processa i quins algorismes hi executa.
  * Reduir l'obsolescència de les tecnologies implantades i la generació de residus tecnològics.
  * Compartir coneixement i costos entre ajuntaments i amb entitats de l'economia social a l'hora d'implantar sistemes tecnològics. No partir de zero en cada sistema implantat.
  * Potenciar l'ús de programari lliure per part dels ajuntaments.
* Promoure l'ús de programari lliure a la societat    
  * Promoure la innovació i la creació d'un teixit tecnològic local basat en tecnologies lliures
  * Invertir recursos públics en mantenir, cuidar i fer créixer el procomú digital.
> es podrien unificar els diversos punts en un o dos? Tenir molts punts fa més feixuga la lectura i li treuen protagonisme als punts programàtics

#### Proposta programàtica
1. Vetllar per a que totes les webs, apps o plataformes tecnològiques municipals, funcionin perfectament sobre plataformes lliures (navegadors, escriptoris, sistemes operatius), sense requerir la instal·lació de cap component no lliure.
1. Basar els nous desenvolupaments en tecnologies lliures, sempre que sigui possible. Publicar-los sota una llicència lliure, i aïllar aquelles parts que transitòriament hagin de fer ús de tecnologies privatives en forma de components substituïbles amb interfícies ben documentades. TODO: desenvolupaments tecnològics i estudis tècnics en general (Muriel).
1. Garantir que tot el maquinari es compripugui funcionar sense inconvenients sobre programari lliure (TODO: firmware, BIOS).
1. Documentar els desenvolupaments publicats per tal que qualsevol persona o entitat amb els coneixements tècnics necessaris els puguin reutilitzar.
1. Contribuir totes les millores i modificacions realitzades a productes de programari lliure existents al projecte original, procurant que siguin acceptades abans de fer-les servir (upstream first).
1. Respectar les pràctiques i els codis de conducta, explícits o no, de les comunitats en que es participa.
1. Crear mecanismes de coordinació entre administracions i entitats de l'economia social que permetin mancomunar desenvolupaments. TODO: Fomentar processos de disseny, desenvolupament i implementació de codi que resolguin necessitats comunes, mitjançant la cooperació, la coproducció i la compartició de coneixements i recursos.
1. TODO: alguna cosa sobre formació del personal administració.
1. TODO: alguna cosa sobre implantació de PL als ajuntaments (GNU/Linux, Libreoffice, objectiu 2.e).

### Política de dades
> falta introducció


#### Objectiu general de l'àmbit
* Fer l'administració més transparent i propera a la ciutadania.
* Facilitar l'accés i explotació dels conjunts de dades públics.
* Fer créixer les col·leccions de dades de lliure accés.
* Garantir, amb criteris ètics, la privacitat de les dades de caràcter personal, administrant-les de manera més conscient, i reduint els riscos derivats de la seva explotació.


#### Proposta programàtica

##### Open data per defecte
1. Publicar sota llicències lliures i formats reutilitzables tota la informació pública generada o gestionada per l'administració local (des dels pressupostos municipals fins als temps dels semàfors, p.e.).
1. Estandaritzar els formats de les dades publicades entre diferents ajuntaments i administracions, facilitant així l'anàlisi creuat de les dades.
1. Publicar les dades crues generades per a la realització d'estudis finançats amb diners públics, de manera que es puguin comprovar els estudis o fer-ne derivats.
1. Promoure la reutilització d'aquestes dades, mitjançant una publicitat activa, una bona accessibilitat al web municipal, i l'especificació de les llicències.

##### Privacitat de les dades de caràcter personal
1. Administrar les dades de caràcter personal únicament dins de l'administració i seguint estrictes criteris de seguretat, garantint els mitjans tècnics i els coneixements per gestionar-les sense dependre de tercers.
1. Limitar la recollida de dades de caràcter personal a aquells casos en què prèviament s'han establert les finalitats concretes per a les que seran utilitzades, minimitzant, així, la quantitat de dades personals que es recullen.
1. Implementar mecanismes per donar un màxim de serveis per conèixer, corregir o esborrar dades personals dels sistemes de l'administració; i no només el mínim legal que exigeix la GDPR.

### Democratització de la tecnologia
> @geoinquiet proposa afegir aquests punts en la introducció d'aquest apartat:
> * El fàcil accés de la ciutadania a la tecnologia, especialment la relativa a la connectivitat a Internet i a l'ús de telefons inteligents, comporta una transformació de la nostra societat cap a una nova cultura digital.
> * La democratització de la tecnologia, en termes d'abaratiment i facilitat de connectivitat, permeten la creació de nous serveis des de l'administració particularitzats a les necessitats de la seva ciutadania i accessibles des del telèfon sense necessitats presencials, ni paperasa.
> * La cultura digital ha d'impregnar la generació de serveis a la ciutadania, atès que signifiquen un abaratiment dels costos de prestació i faciliten l'accés als serveis.


#### objectiu general de l'àmbit
* Posar a l’abast de tothom les innovacions tecnològiques que l'administració incorpora en la seva acció municipal.
* Pal·liar la distància creada a partir dels diferents usos tecnològics per part dels diferents sectors de la societat (escletxa digital), tant per accés com per coneixement.
* Vetllar per un accés no discriminador a tràmits i accions digitals de l'administració.
* Acompanyar tota acció que impliqui una interacció digital amb la ciutadania de la formació necessària per a que tot tècnic municipal pugui guiar a la ciutadania.
* Promoure una relació conscient i crítica de la ciutadania amb la tecnologia.
> es podrien unificar els diversos punts en un o dos? Tenir molts punts fa més feixuga la lectura i li treuen protagonisme als punts programàtics

#### Proposta programàtica
1. Posar en marxa programes de formació i capacitació digital per a la ciutadania, així com reforçar els existents.
1. Promoure espais de connexió i ús tecnològic que ofereixin eines i recursos de manera gratuïta, així com dotar de recursos als existents (biblioteques, centres cívics o telecentres).
1. Oferir programes pedagògics en coordinació amb centres educatius de diferents nivells que permetin abordar com treballar la tecnologia de manera inclusiva i respectuosa amb la ciutadania.
1. Oferir formació tecnològica (inicial i avançada) a col·lectius amb risc d’exclusió social per motius socioeconòmics, en temàtiques d’innovació tecnològica, en coordinació amb els serveis socials municipals.
1. Garantir que el personal de l'administració podrà acompanyar a la ciutadania en la seva interacció digital amb l'administació a partir de programes de formació interna.

### Compra pública de dispositius electrònics i circularitat

#### objectiu general de l'àmbit
 * Tendir cap a compra de dispositius electrònics que minimitzi l’impacte negatiu en les persones i el medi ambient.

#### Proposta programàtica
 1. Promoció de la reducció de l’impacte medi-ambiental i l’economia social i solidaria.
 1. Respecte als drets laborals dels treballadors involucrats en la fabricació, manteniment, recollida i reciclatge de dispositius electrònics.
 1. Compra pública responsable que respecti els drets laborals en la producció de béns electrònics.
 1. Traçabilitat en origen dels dispositius per assegurar la circularitat: reparació, renovació, reutilització, garantir el reciclatge final i la transparència de les dades sobre aquests processos.
 1. Compromís de donació dels dispositius a entitats socials al final de la seva primera vida útil, per allargar la seva vida útil i crear beneficis socials amb la reutilització.
 1. Compra pública amb extensió de garantia que inclogui reparació i manteniment inclòs que permeti estendre la vida útil dels dispositius.
 1. Promoció del coneixement sobre la reparació dels dispositius als centres educatius (escoles, universitats).


### Estandars lliures
*  establir polítiques actives d’ús d’estàndards oberts i de migració a programari lliure, havent establert una llista d’aplicacions homologades per a cada ús i funcionalitat requerides pel sector públic, i habilitar estratègies de reutilització, col·laboració i compartició d’esforços amb altres entitats públiques del món.
